import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { ProgressBarComponent } from './modules/game/components/progress-bar/progress-bar.component';
import { InstructionsComponent } from './modules/game/components/instructions/instructions.component';
import { GameComponent } from './modules/game/components/game/game.component';
import { AppRoutingModule } from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';

import {MaterialModule} from '../material';
import {MatSlideToggleModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import { PlayerStatusComponent } from './modules/game/components/player-status/player-status.component';
import { DevelopersComponent } from './modules/game/components/developers/developers.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { TaskListComponent } from './modules/game/components/task-list/task-list.component';
import { TaskComponent } from './modules/game/components/task/task.component';
import { ResultsComponent } from './modules/game/components/results/results.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProgressBarComponent,
    InstructionsComponent,
    GameComponent,
    PlayerStatusComponent,
    DevelopersComponent,
    TaskListComponent,
    TaskComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
