import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {GameComponent} from './modules/game/components/game/game.component';
import {InstructionsComponent} from './modules/game/components/instructions/instructions.component';
import {ResultsComponent} from './modules/game/components/results/results.component';


const routes: Routes = [
  { path: 'instructions', component: InstructionsComponent},
  { path: 'game', component: GameComponent},
  { path: 'results', component: ResultsComponent},
  { path: '', redirectTo: '/instructions', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
