import {Component, Input, OnInit} from '@angular/core';
import {Task, TaskState} from '../../models/task';
import {ValueDeliveredService} from '../../services/value-delivered.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input()
  public task: Task;
  interval;
  public urgencyColor = '#FFF';

  constructor(private valueDeliveredService: ValueDeliveredService) {

  }

  ngOnInit() {
    this.updateTaskListUrgency();
    this.startTaskTimer();
  }

  startTaskTimer() {
    this.interval = setInterval(() => {
      this.updateTaskListUrgency();
    }, Task.URGENCY_TIME);
  }

  /**
   * Updates urgency of tasks within the backlog
   */
  private updateTaskListUrgency() {

    // Don't update urgency if we're actively working on it
    if (this.task.state !== TaskState.BACKLOG) {
      this.urgencyColor = '#FFF';
      return;
    }

    this.task.currentUrgencyTimer -= Task.URGENCY_TIME;
    this.task.increaseUrgency();
    this.updateUrgencyStyles();
  }

  /**
   * Update the background color depending on how urgently the task need to be complete
   */
  private updateUrgencyStyles() {
    if (this.task.currentUrgencyTimer < Task.MAX_URGENCY * 0.75) {
      this.urgencyColor = '#FAAAAA';
    }

    if (this.task.currentUrgencyTimer < Task.MAX_URGENCY * 0.5) {
      this.urgencyColor = '#D46A6A';
    }

    if (this.task.currentUrgencyTimer < Task.MAX_URGENCY * 0.25) {
      this.urgencyColor = '#AA3939';
    }

    if (this.task.currentUrgencyTimer < Task.MAX_URGENCY * 0.1) {
      this.urgencyColor = '#801515';
    }

    if (this.task.currentUrgencyTimer <= 0 && this.task.state !== TaskState.FAILED) {
      this.task.state = TaskState.FAILED;
      this.valueDeliveredService.addLoss(this.task.currentValue);
    }
  }

  isTaskFailed() {
    return this.task.state === TaskState.FAILED;
  }
}
