import {Component, OnInit} from '@angular/core';
import {CdkDragDrop} from '@angular/cdk/drag-drop/typings/drag-events';
import {moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Developer, DeveloperStatus} from '../../models/developer';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.css']
})
export class DevelopersComponent implements OnInit {

  developers: Array<Developer> = [];
  startingDevelopers = 5; // Initial developer count

  // Draggable area arrays to hold the developers
  analyzing: Array<Developer> = [];
  working: Array<Developer> = [];
  testers: Array<Developer> = [];



  constructor() {
    // Init developer status
    this.assignDevelopers();
  }

  ngOnInit() {

  }

  /**
   * Distributes developers across all three groups and gives the developers their initial status
   */
  assignDevelopers() {
    // Evenly distribute all developers
    for (let i = 0; i < this.startingDevelopers; i++) {
      const position = i % 3;
      let developer;
      switch (position) {
        case 0:
          developer = new Developer(DeveloperStatus.ANALYZING);
          this.analyzing.push(developer);
          break;
        case 1:
          developer = new Developer(DeveloperStatus.WORKING);
          this.working.push(developer);
          break;
        case 2:
          developer = new Developer(DeveloperStatus.TESTING);
          this.testers.push(developer);
          break;
      }

      this.developers.push(developer);


    }
  }

  /**
   * Developer drag and drop event
   * Handles changing the developer status when changed
   *
   * @param ev Developer drop event
   */
  drop(ev: CdkDragDrop<Developer[]>) {

    if (ev.previousContainer === ev.container) {
      // This isn't really important but just change the developer position
      moveItemInArray(ev.container.data, ev.previousIndex, ev.currentIndex);
    } else {
      // Transfer developer into the new container
      transferArrayItem(ev.previousContainer.data,
        ev.container.data,
        ev.previousIndex,
        ev.currentIndex
      );

      // Depending on which container the developer is dropped into, change the  status of the developer
      // object
      switch (ev.container.id) {
        case 'cdk-drop-list-0':
          ev.container.data[ev.currentIndex].currentAction = DeveloperStatus.ANALYZING;
          break;
        case 'cdk-drop-list-1':
          ev.container.data[ev.currentIndex].currentAction = DeveloperStatus.WORKING;
          break;
        case 'cdk-drop-list-2':
          ev.container.data[ev.currentIndex].currentAction = DeveloperStatus.TESTING;
          break;
      }

    }
  }

}
