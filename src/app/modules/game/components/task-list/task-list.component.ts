import {Component, OnInit} from '@angular/core';
import {Task, TaskState} from '../../models/task';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {ValueDeliveredService} from '../../services/value-delivered.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  tasks: Array<Task> = [];
  public initialTaskCount = Math.ceil(Math.random() * 20 + 1);
  private currentTasks: number;
  private interval;
  // Draggable area arrays to hold the tasks
  backlogTasks: Array<Task> = [];
  analyzingTasks: Array<Task> = [];
  workingTasks: Array<Task> = [];
  testingTasks: Array<Task> = [];
  finishedTasks: Array<Task> = [];

  analyzingEfficiency = 0.0;
  workingEfficiency = 0.0;
  testingEfficiency = 0.0;

  constructor(private valueService: ValueDeliveredService) {
    this.createInitialTasks();
  }

  ngOnInit() {
  }

  /**
   * Creates the initial backlog of tasks for the user
   */
  createInitialTasks() {
    this.currentTasks = this.initialTaskCount;
    for (let i = 0; i < this.initialTaskCount; i ++) {
      this.generateTask(i);
    }
  }

  public generateTask(id: number) {
    const taskHours = Math.ceil(Math.random() * Task.MAX_HOURS);
    const taskValue = Math.ceil(Math.random() * Task.MAX_VALUE);
    const taskUrgencyValue = Math.ceil(Math.random() * Task.MAX_URGENCY + 2000);
    const task = new Task(id, taskHours, taskValue, taskUrgencyValue);
    this.backlogTasks.push(task);
    this.tasks.push(task);
  }

  drop(event: CdkDragDrop<Task[]>) {

    if (event.previousContainer === event.container) {
      // Change index depending on where the item is place
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      // Don't want to move if we still have hours left
      if (event.previousContainer.data[event.previousIndex].state !== TaskState.BACKLOG) {
        if (event.previousContainer.data[event.previousIndex].hoursLeft > 0) {
          return;
        }
      }

      // Update the current state of the task
      event.previousContainer.data[event.previousIndex].changeState();
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      if (event.container.id === 'cdk-drop-list-7') {
        this.valueService.addValue(event.container.data[event.currentIndex].currentValue);
      }
    }
  }



}
