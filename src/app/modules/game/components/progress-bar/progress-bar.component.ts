import { Component, OnInit } from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

export enum progress {
  INSTRUCTIONS,
  GAME,
  EVALUATION,
  REPLAY
}


@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {


  currentProgress: progress;

  constructor(private router: Router) {

    // Listen for route changes so we can update the progress bar accordingly
    router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        // Only want to change after navigation has ended
        const val = ev.url;
        switch (val) {
          case '/instructions':
            this.currentProgress = progress.INSTRUCTIONS;
            break;
          case '/game':
            this.currentProgress = progress.GAME;
            break;
        }
      }
    });
  }

  ngOnInit() {

  }

  get progress() {
    return progress;
  }

}
