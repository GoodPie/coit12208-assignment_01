import { Component, OnInit } from '@angular/core';
import {ValueDeliveredService} from '../../services/value-delivered.service';

@Component({
  selector: 'app-player-status',
  templateUrl: './player-status.component.html',
  styleUrls: ['./player-status.component.css']
})
export class PlayerStatusComponent implements OnInit {

  constructor(private valueService: ValueDeliveredService) { }

  ngOnInit() {
  }

}
