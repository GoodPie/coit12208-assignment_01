import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription, timer} from 'rxjs';
import {Game} from '../../models/game';
import {Developer} from '../../models/developer';
import {Task} from '../../models/task';
import {by} from 'protractor';
import {DevelopersComponent} from '../developers/developers.component';
import {TaskListComponent} from '../task-list/task-list.component';
import {Router} from '@angular/router';
import {ResultsService} from '../../services/results.service';
import {ValueDeliveredService} from '../../services/value-delivered.service';



@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit, OnDestroy {


  @ViewChild(DevelopersComponent, {static: false})
  private developersComponent: DevelopersComponent;
  @ViewChild(TaskListComponent, {static: false})
  private taskListComponent: TaskListComponent;
  private timerSubscription: Subscription;
  private currentTime = 1;
  private currentDay = 1;
  private currentSprint = 0;
  private isPaused = false;

  constructor(private router: Router, private resultsService: ResultsService, private valueService: ValueDeliveredService) {
  }

  ngOnInit() {
    this.startGameTimer();
  }

  private startGameTimer() {
    // Init timer
    const gameTimer = timer(1000, 1000);

    // Callback on timer change
    this.timerSubscription = gameTimer.subscribe(time => {
      if (this.isPaused) {
        // Game is paused so don't progress timer any further
        return;
      }

      // TODO: Implement game end based on current sprint
      this.currentTime += 1;

      if (this.currentTime % Game.HOURS_IN_DAY === 0) {
        this.currentDay += 1;
      }

      if (this.currentDay % Game.SPRINT_DURATION === 0) {
        this.currentSprint += 1;
        this.currentTime = 1;
        this.currentDay = 1;
      }

      if (this.currentSprint > Game.SPRINT_AMOUNT) {
        this.showResultsPage();
      }

      if (Math.random() * 10 < 5) {
        this.taskListComponent.initialTaskCount += 1;
        this.taskListComponent.generateTask(this.taskListComponent.initialTaskCount);
        console.log('Task generated');
      }


      // Update the efficiencies in the task list so we can display them
      this.taskListComponent.analyzingEfficiency =
        this.updateTasks(this.developersComponent.analyzing, this.taskListComponent.analyzingTasks);
      this.taskListComponent.workingEfficiency =
        this.updateTasks(this.developersComponent.working, this.taskListComponent.workingTasks);
      this.taskListComponent.testingEfficiency =
        this.updateTasks(this.developersComponent.testers, this.taskListComponent.testingTasks);
    });
  }

  private showResultsPage() {

    // Get all the values we need
    const totalUpdates = this.currentDay * Game.HOURS_IN_DAY * this.currentSprint;
    const analyzeAvgEfficiency = this.taskListComponent.analyzingEfficiency / totalUpdates;
    const workingAvgEfficiency = this.taskListComponent.workingEfficiency / totalUpdates;
    const testingAvgEfficiency = this.taskListComponent.testingEfficiency / totalUpdates;
    const valueDelivered = this.valueService.getValue();
    const valueLost = this.valueService.getValueLost();

    // Share variables with service so we have access to them in the results page
    this.resultsService.setEfficiencies(analyzeAvgEfficiency, workingAvgEfficiency, testingAvgEfficiency);
    this.resultsService.setValues(valueLost, valueDelivered);

    this.router.navigateByUrl('/results');
  }

  /**
   * Updates the tasks within a certain list and returns how efficiently the developers are working on them
   *
   * @param developers Developers who are working on the tasks
   * @param tasks List of tasks to be worked on
   */
  updateTasks(developers: Developer[], tasks: Task[]) {
    const devCount = developers.length;
    // Need to determine whether developers are working on tasks with 0 hours or not so we can apply this to the calculations
    let taskCount = 0;
    for (const task of tasks) {
      if (task.hoursLeft > 0.0) {
        taskCount += 1;
      }
    }

    // No point proceeding as there is no tasks to work on
    if (taskCount === 0 && devCount === 0) { return 1; }
    if (taskCount !== 0 && devCount === 0) { return 0; }

    // Just use a 0-1 score for efficiency, representing percentage of an hour
    let efficiency = devCount / taskCount;
    if (devCount > taskCount) {
      efficiency = taskCount / devCount;
    }
    if (efficiency > 1) {
      efficiency = 1;
    }

    // Negate the efficiency from the task
    for (const task of tasks) {
      task.hoursLeft -= efficiency;
      if (task.hoursLeft < 0.00) {
        task.hoursLeft = 0;
      }
    }

    return efficiency;
  }

  ngOnDestroy(): void {
    this.timerSubscription.unsubscribe();
  }

}
