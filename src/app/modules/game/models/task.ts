/**
 * Task
 */
export class Task {

  static readonly MAX_HOURS = 48;
  static readonly MAX_VALUE = 100;
  static readonly URGENCY_TIME = 10000;
  static readonly MAX_URGENCY = 100000;

  id: number; // Incrementing ID
  totalHours: number; // Hours to complete the task
  hoursLeft: number; // Hours remaining on the current task
  state: TaskState = TaskState.BACKLOG; // Current state
  currentValue: number;
  currentUrgencyTimer: number;

  constructor(id: number, totalHours: number, initialValue: number, urgency: number) {
    this.id = id;
    this.totalHours = totalHours;
    this.hoursLeft = totalHours;
    this.state = TaskState.BACKLOG;
    this.currentValue = initialValue;
    this.currentUrgencyTimer = urgency;
  }

  increaseUrgency() {
    this.currentValue -= this.currentValue * 0.10; // Arbitrary percentage decrease in value
    this.currentUrgencyTimer -= Task.URGENCY_TIME;

  }

  changeState() {
    switch (this.state) {
      case TaskState.BACKLOG:
        this.state = TaskState.ANALYZING;
        break;
      case TaskState.ANALYZING:
        this.state = TaskState.ANALYZE_FINISHED;
        break;
      case TaskState.ANALYZE_FINISHED:
        this.state = TaskState.IN_PROGRESS;
        break;
      case TaskState.IN_PROGRESS:
        this.state = TaskState.FINISHED;
        break;
      case TaskState.FINISHED:
        this.state = TaskState.TESTING;
        break;
      case TaskState.TESTING:
        this.state = TaskState.TESTING_FINISHED;
        break;
      case TaskState.TESTING_FINISHED:
        this.state = TaskState.DONE;
        break;
    }

    if (this.state !== TaskState.DONE) {
      this.hoursLeft = this.totalHours;
    }
  }
}

export enum TaskState {
  BACKLOG,
  ANALYZING,
  ANALYZE_FINISHED,
  IN_PROGRESS,
  FINISHED,
  TESTING,
  TESTING_FINISHED,
  DONE,
  FAILED
}
