/**
 * Developer object
 * Just used to store the developer status and the developer image location
 */
export class Developer {

  currentAction: DeveloperStatus;
  image = 'assets/img/developer.png';


  constructor(currentAction: DeveloperStatus) {
    this.currentAction = currentAction;
  }

  getPictureAsHTML() {
    return '<img src="' + this.image + '" alt="" height="50px">';
  }

}

export enum DeveloperStatus {
  ANALYZING,
  WORKING,
  TESTING
}
