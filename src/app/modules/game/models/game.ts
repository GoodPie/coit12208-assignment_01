export class Game {

  // Amount of hours in a working day. Used for sprints
  static readonly HOURS_IN_DAY = 12;

  // Amount of days in a sprint
  static readonly SPRINT_DURATION = 5;

  // Amount of sprints in a game
  static readonly SPRINT_AMOUNT = 6;

}
