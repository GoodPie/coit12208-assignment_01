import { TestBed } from '@angular/core/testing';

import { ValueDeliveredService } from './value-delivered.service';

describe('ValueDeliveredService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValueDeliveredService = TestBed.get(ValueDeliveredService);
    expect(service).toBeTruthy();
  });
});
