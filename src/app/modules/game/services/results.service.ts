import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  private analyzeEfficiency = 0.0;
  private workingEfficiency = 0.0;
  private testEfficiency = 0.0;
  private valueDelivered = 0.0;
  private valueLost = 0.0;

  constructor() { }

  setValues(valueLost: number, valueDelivered: number) {
    this.valueLost = valueLost;
    this.valueDelivered = valueDelivered;
  }

  setEfficiencies(analyze: number, working: number, testing: number) {
    this.analyzeEfficiency = analyze;
    this.workingEfficiency = working;
    this.testEfficiency = testing;
  }

  getAnalyzingEfficiency() {
    return this.analyzeEfficiency * 100;
  }

  getWorkingEfficiency() {
    return this.workingEfficiency * 100;
  }

  getTestingEfficiency() {
    return this.testEfficiency * 100;
  }

  getValueDelivered() {
    return this.valueDelivered;
  }

  getValueLost() {
    return this.valueLost;
  }
}
