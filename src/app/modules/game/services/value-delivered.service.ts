import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValueDeliveredService {

  private valueDelivered = 0.0;
  private valueLost = 0.0;


  addValue(value: number) {
    this.valueDelivered += value;
  }

  addLoss(value: number) {
    this.valueLost += value;
  }

  getValue() {
    return this.valueDelivered;
  }

  getValueLost() {
    return this.valueLost;
  }
}
